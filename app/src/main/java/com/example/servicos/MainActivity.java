package com.example.servicos;

import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    TextView txtApostas;

    class ApostasReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            int[] numeros = intent.getIntArrayExtra("numeros");
            txtApostas.append("\n" + Arrays.toString(numeros));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ApostasReceiver ar = new ApostasReceiver();
        registerReceiver(ar, new IntentFilter(ServicoApostas.ACTION_RESPOSTA_SERVICO_APOSTAS));

        txtApostas = (TextView) findViewById(R.id.txtApostas);
    }

    public void iniciar(View view) {
        Intent intent = new Intent(getApplicationContext(), ServicoApostas.class);

        intent.putExtra("quantidade", 6);

        startService(intent);
    }
}