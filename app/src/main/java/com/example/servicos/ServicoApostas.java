package com.example.servicos;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;

import java.util.Random;

public class ServicoApostas extends IntentService {
    public static String ACTION_RESPOSTA_SERVICO_APOSTAS = "resposta_servico_apostas";

    public ServicoApostas() {
        super("ServicoApostas");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            Random random = new Random(System.currentTimeMillis());
            int quantidade = intent.getIntExtra("quantidade", 10);
            int cont = 0;
            int[] nums = new int[quantidade];

            System.out.println("Iniciado geração dos números");
            while (cont < quantidade) {
                nums[cont] = random.nextInt(1000);
                cont++;
                try {
                    Thread.sleep(1000);
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }

            Intent resposta = new Intent(ACTION_RESPOSTA_SERVICO_APOSTAS);
            resposta.putExtra("numeros", nums);
            sendBroadcast(resposta);
            System.out.println("Finalizado geração dos números");
        }
    }
}